// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace seidor.iOS
{
    [Register ("SimpleBackgroundTransferViewController")]
    partial class SimpleBackgroundTransferViewController
    {
        //[Outlet]
        //UIKit.UIBarButtonItem crashButton { get; set; }


        [Outlet]
        UIKit.UIImageView imageView { get; set; }


        [Outlet]
        UIKit.UIProgressView progressView { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem startButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel txtDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel txtText { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (progressView != null) {
                progressView.Dispose ();
                progressView = null;
            }

            if (startButton != null) {
                startButton.Dispose ();
                startButton = null;
            }

            if (txtDescription != null) {
                txtDescription.Dispose ();
                txtDescription = null;
            }

            if (txtText != null) {
                txtText.Dispose ();
                txtText = null;
            }
        }
    }
}