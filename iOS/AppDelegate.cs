﻿using System;
using Foundation;
using UIKit;

namespace seidor.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(UIApplication.BackgroundFetchIntervalMinimum);

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());



            return base.FinishedLaunching(app, options);
        }

        //start
		public Action BackgroundSessionCompletionHandler { get; set; }

		public override UIWindow Window { get; set; }

		public override void HandleEventsForBackgroundUrl(UIApplication application, string sessionIdentifier, Action completionHandler)
		{
			Console.WriteLine("HandleEventsForBackgroundUrl");
			BackgroundSessionCompletionHandler = completionHandler;
		}

		public override void OnActivated(UIApplication application)
		{
			Console.WriteLine("OnActivated");
		}

		public override void OnResignActivation(UIApplication application)
		{
			Console.WriteLine("OnResignActivation");
		}
        //end

    }
}
