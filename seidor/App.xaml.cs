﻿using System.Collections.Generic;

using Xamarin.Forms;

namespace seidor
{
    public partial class App : Application
    {
        public static bool UseMockDataStore = true;
        public static string BackendUrl = "http://onfire.zerodemons.com";

        public static IDictionary<string, string> LoginParameters => null;

        public App()
        {
            InitializeComponent();

            if (!UseMockDataStore)
                DependencyService.Register<MockDataStore>();
            else
                DependencyService.Register<CloudDataStore>();

            GoToMainPage();
        }

        public static void GoToMainPage()
        {
            Current.MainPage = new TabbedPage
            {
                Children = {
                    new NavigationPage(new ItemsPage())
                    {
                        Title = "Browse",
                        Icon = (Device.RuntimePlatform == Device.iOS)? "tab_feed.png":null
                    },
                    new NavigationPage(new AboutPage())
                    {
                        Title = "About",
                        Icon = (Device.RuntimePlatform == Device.iOS)? "tab_about.png":null
                    },
                }
            };
        }
    }
}
