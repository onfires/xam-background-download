﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace seidor.Controls
{
    public partial class ItemCell : ContentView
    {
        public ItemCell()
        {
            InitializeComponent();
        }

		public static readonly BindableProperty TextProperty =
         BindableProperty.Create("Text", typeof(string), typeof(ItemCell), "");

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly BindableProperty DescriptionProperty =
          BindableProperty.Create("Description", typeof(string), typeof(ItemCell), "");

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly BindableProperty ImageProperty =
          BindableProperty.Create("Image", typeof(string), typeof(ItemCell), "ic_file_download_48pt.png");

        public string Image
        {
            get { return (string)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }
	}
}

/*public ItemCellx()
        {
            Label Text = new Label();
            Label Description = new Label();
            Image Image = new Image();
            Grid grid = new Grid
            {
                Padding = new Thickness(10),
                RowDefinitions = {
                    new RowDefinition{ Height = GridLength.Auto},
                    new RowDefinition{ Height = GridLength.Auto},
                },
                ColumnDefinitions = {
                    new ColumnDefinition{ Width = new GridLength(4,GridUnitType.Star)},
                    new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star)},
                }
            };
            grid.Children.Add(Text,0,0);
            grid.Children.Add(Description, 0, 1);
            grid.Children.Add(Image, 1, 1);
            //Grid.SetRowSpan(Image,2);
            View = grid;


            //Binding data
            //Text.SetBinding<Item>(Label.TextProperty, i => i.Text);
            //Description.SetBinding<Item>(Label.TextProperty, i => i.Description);

        }*/
