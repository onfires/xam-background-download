﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace seidor
{
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();

        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {

            var item = args.SelectedItem as Item;
            if (item == null)
                return;

#if __IOS__

            var storyboard = UIKit.UIStoryboard.FromName("MainStoryboard", null);
            var vc = storyboard.InstantiateViewController("transferViewControllerId") as seidor.iOS.SimpleBackgroundTransferViewController;

            vc.fileName = item.Text;
            vc.downloadUrlString = item.Description;



            var navigationController = new UIKit.UINavigationController();
            navigationController.PushViewController(vc, false);


            UIKit.UIApplication.SharedApplication.KeyWindow.RootViewController = navigationController;

#endif
#if __ANDROID__
			await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));
#endif
            // Manually deselect item
            ItemsListView.SelectedItem = null;


        }

        async void LoadItems_Clicked(object sender, EventArgs e)
        {
            await viewModel.ExecuteLoadItemsCommand();
            //await Navigation.PushAsync(new NewItemPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            /*
			//Load Items OnAppearing
			if (viewModel.Items.Count == 0)
			    viewModel.LoadItemsCommand.Execute(null);
            //*/
		}
    }
}
