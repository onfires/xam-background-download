﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace seidor
{
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        //public Command CompleteProgressCommand { get; set; }


        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
            //CompleteProgressCommand = new Command(async () => await ExecuteIosCompleteProgressCommand());

        }

		protected override void OnAppearing()
		{
            base.OnAppearing();

#if __ANDROID__
                ExecuteAndroidCompleteProgressCommand();
#endif
		}

#if __ANDROID__
        public /*async Task*/ void ExecuteAndroidCompleteProgressCommand()
		{
           seidor.Droid.DownloadFromUrl download = new seidor.Droid.DownloadFromUrl(Xamarin.Forms.Forms.Context, viewModel.Item.Text, ProgressBarDownload);
           download.Execute(viewModel.Item.Description);
		}
#endif



	}
}
