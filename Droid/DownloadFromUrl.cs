﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using Java.Net;
using Java.IO;
using Android.Util;
using Xamarin.Forms;

namespace seidor.Droid
{
	public class DownloadFromUrl : AsyncTask<string, string, string>
	{
		private ProgressDialog pDialog;
        private String fileName;
		private Context context;
        private Xamarin.Forms.ProgressBar progressBar;

        public DownloadFromUrl(Context context, String fileName, Xamarin.Forms.ProgressBar progressBar)
		{
            this.context = context;
            this.fileName = fileName;
            this.progressBar = progressBar;
		}

		protected override void OnPreExecute()
		{
			base.OnPreExecute();
		}

		protected override void OnProgressUpdate(params string[] values)
		{

			base.OnProgressUpdate(values);
			//pDialog.SetProgressNumberFormat(values[0]);
			//pDialog.Progress = int.Parse(values[0]);
		}
		protected override void OnPostExecute(string result)
		{
			string storagePath = Android.OS.Environment.ExternalStorageDirectory.Path;
            string filePath = System.IO.Path.Combine(storagePath, this.fileName);

             App.Current.MainPage.DisplayAlert(this.fileName, "download done." , "OK"); 

            /*
            //Otra opcion 
            AlertDialog.Builder alert = new AlertDialog.Builder(this.context);
			alert.SetTitle("Done");
			alert.SetMessage("Download done.");
			

			alert.SetNegativeButton("OK", (senderAlert, args) =>
			{
				Toast.MakeText(this.context, "[0.o]?", ToastLength.Short).Show();
			});

			Dialog dialog = alert.Create();
			dialog.Show();
            */

		}
		protected override string RunInBackground(params string[] @params)
		{
			string storagePath = Android.OS.Environment.ExternalStorageDirectory.Path;
            string filePath = System.IO.Path.Combine(storagePath, this.fileName);

            Log.Info("DRoid.BG", "filePath" + filePath);

			int count;
			try
			{
				URL url = new URL(@params[0]);
				URLConnection connection = url.OpenConnection();
                //connection.setRequestProperty("authorization", authHeader);
				connection.Connect();
				int LengthOfFile = connection.ContentLength;
				InputStream input = new BufferedInputStream(url.OpenStream(), LengthOfFile);
				OutputStream output = new FileOutputStream(filePath);

				byte[] data = new byte[1024];
				long total = 0;
                Double por = 0;

				while ((count = input.Read(data)) != -1)
				{
					total += count;
                    por = total * 100 / LengthOfFile ;

                    progressBar.Progress = por/100;
                    //Log.Info("DRoid.WRITE", "writting: " + total + "of: " + LengthOfFile + "%: " + por/100 );
                    PublishProgress("" + (int)por * 100);
                    output.Write(data, 0, count);

				}
				output.Flush();
				output.Close();
				input.Close();

                Log.Info("DRoid.WRITE", "Complete");

                //Toast.MakeText(this.context, "Download done.", ToastLength.Long).Show();
                //DisplayAlert("Done", "Download done..", "OK");
			}

			catch (Exception e)
			{

			}
			return null;
		}
	}
}
